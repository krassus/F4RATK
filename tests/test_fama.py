from pandas import PeriodIndex, PeriodDtype
from pandas.tseries import offsets

from f4ratk.fama import FamaReader, Region, Precision


class TestFamaReader:
    def given_monthly_precision_should_return_data_with_monthly_period_index(self):
        returns = FamaReader().fama_data(region=Region.EU, precision=Precision.MONTHLY)

        assert isinstance(returns.index, PeriodIndex)
        assert returns.index.dtype == PeriodDtype(freq=offsets.MonthEnd())


    def given_daily_precision_should_return_data_with_daily_period_index(self):
        returns = FamaReader().fama_data(region=Region.EU, precision=Precision.DAILY)

        assert isinstance(returns.index, PeriodIndex)
        assert returns.index.dtype == PeriodDtype(freq=offsets.BusinessDay())