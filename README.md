# F4RATK

[![Build status](https://img.shields.io/appveyor/build/toroettg/f4ratk)](https://ci.appveyor.com/project/toroettg/f4ratk)
[![License: MIT](https://img.shields.io/badge/License-MIT-informational.svg)](https://opensource.org/licenses/MIT)

A Fama/French Finance Factor Regression Analysis Toolkit.

## Here be dragons

This project is experimental: it does not provide any guarantees and its
results are not rigorously tested. It should not be used by itself as a
basis for decision‐making.

If you would like to join, please submit pull requests to improve the work here.

## Quickstart

This project uses Python 3.8 and manages dependencies via pipenv.
There are no binary distributions at the moment; the source code
needs to be downloaded and run manually.  

### Setup

The following downloads the project and installs all required dependencies.

```
git clone https://codeberg.org/toroettg/F4RATK.git
cd F4RATK
pipenv install
```

### Usage

Adjust the *f4ratk/f4ratk.py* file according to your problem.
Then run the regression analysis as follows.

 `pipenv run python -m f4ratk.f4ratk`

## License

This project is licensed under the MIT License.
See the [LICENSE] file for the full license text.

## Notice

Based on the works of [Rand Low] and [DD].

[LICENSE]: https://codeberg.org/toroettg/F4RATK/src/branch/main/LICENSE

[Rand Low]: https://randlow.github.io/posts/finance-economics/asset-pricing-regression
[DD]: https://www.codingfinance.com/post/2019-07-01-analyze-ff-factor-python
