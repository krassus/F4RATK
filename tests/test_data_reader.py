from datetime import date, timedelta
from pathlib import Path
from tempfile import NamedTemporaryFile
from typing import Iterable

from numpy import dtype

from f4ratk.data_reader import csv_reader, fama_french_reader, fred_reader, yahoo_reader


class TestFamaReader:
    def should_create_fama_reader_for_given_returns_data_name(self):
        result = fama_french_reader(returns_data='Developed_5_Factors_Daily')
        assert result.symbols == 'Developed_5_Factors_Daily'

    def should_create_fama_reader_with_cache(self):
        result = fama_french_reader(returns_data='')

        assert result.session._cache_expire_after == timedelta(days=14)
        assert result.session._cache_name == 'cache'

    def should_query_full_data_range(self):
        result = fama_french_reader(returns_data='')

        assert result.start == date(1970, 1, 1)


class TestYahooReader:
    def should_create_yahoo_reader_for_given_ticker_symbol_and_date_range(self):
        result = yahoo_reader(ticker_symbol="V6IC.DE", start=date(2014, 1, 1),
                              end=date(2019, 12, 31))
        assert result.symbols == 'V6IC.DE'
        assert result.start == date(2014, 1, 1)
        assert result.end == date(2019, 12, 31)


class TestFredReader:
    def should_create_fred_reader_for_given_exchange_symbol_and_date_range(self):
        result = fred_reader(exchange_symbol='DEXUSEU', start=date(2014, 1, 1),
                             end=date(2019, 12, 31))
        assert result.symbols == 'DEXUSEU'
        assert result.start == date(2014, 1, 1)
        assert result.end == date(2019, 12, 31)


class TestCsvFileReader:
    def when_read_file_with_content(self, lines: Iterable[str]):
        pass

    def should_parse_isodate_and_returns_column(self):
        with NamedTemporaryFile(mode='w+', prefix='f4ratk_test_tmp_') as file:
            print(*(
                '2020-11-18,"15.35"',
                '2020-11-17,14.33'
            ),
                  sep='\n', end='\n', file=file)

            file.flush()

            result = csv_reader(path=Path(file.name)).read()

        assert len(result) == 2
        assert result.index.dtype == dtype('datetime64[ns]')
        assert result.index.name == 'Dates'
        assert result.columns == ['Returns']

        assert result['Returns']['2020-11-18'][0] == 15.35
        assert result['Returns']['2020-11-17'][0] == 14.33
