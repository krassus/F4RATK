from datetime import date
from enum import Enum, unique

import pandas as pd

from f4ratk.data_reader import CsvFileReader
from f4ratk.exchange import Currency, ExchangeReader
from f4ratk.fama import Precision


@unique
class ValueFormat(Enum):
    PRICE = 'Price'
    RETURNS = 'Returns'


class FileReader:
    def __init__(
        self,
        csv_reader: CsvFileReader,
        exchange_reader: ExchangeReader,
        currency: Currency,
        value_format: ValueFormat
    ):
        self._csv_reader = csv_reader
        self._exchange_reader = exchange_reader
        self._currency = currency
        self._value_format = value_format

    def read(
        self,
        start: date = None,
        end: date = None,
        precision=Precision.DAILY
    ) -> pd.DataFrame:
        data = self._csv_reader.read().sort_index()
        data.index = pd.to_datetime(data.index)
        data = self._convert_index_to_periods(data, precision=precision)

        if self._currency is not Currency.USD:
            data = self._convert_currency(
                data=data,
                currency=self._currency,
                start=start,
                end=end
            )

        if self._value_format == ValueFormat.PRICE:
            data = data[['Returns']].pct_change() * 100

        return data.dropna()

    def _convert_currency(
        self,
        data: pd.DataFrame,
        currency: Currency,
        start: date,
        end: date
    ) -> pd.DataFrame:
        exchange_data = self._exchange_reader.exchange_data(
            currency,
            start,
            end
        )

        data = pd.merge(
            data,
            exchange_data,
            left_index=True,
            right_index=True
        )

        data['Returns'] = (
            data['Returns'] *
            data[ExchangeReader.EXCHANGE_RATE_COLUMN]
        )

        return data[['Returns']]

    def _convert_index_to_periods(
        self,
        data: pd.DataFrame,
        precision: Precision
    ) -> pd.DataFrame:
        frequency = self._precision_to_frequency(precision)
        return data.to_period(freq=frequency)

    def _precision_to_frequency(self, precision: Precision) -> str:
        if precision == Precision.DAILY:
            return 'B'
        elif precision == Precision.MONTHLY:
            return 'M'

        raise NotImplementedError
