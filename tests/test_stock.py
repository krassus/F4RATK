from unittest.mock import Mock

from pandas import PeriodIndex, PeriodDtype, DataFrame, DatetimeIndex
from pandas.tseries import offsets
from pytest import approx
from pytest_mock import MockerFixture

from f4ratk.exchange import Currency
from f4ratk.stock import StockReader, Stock


class TestStockReader:
    def given_any_ticker_symbol_when_reading_stock_data_should_return_data_with_business_day_precision(self):
        returns = StockReader(exchange_reader=None) \
            .stock_data(
            stock=Stock(ticker_symbol='EUNL.DE', currency=Currency.USD),
            start=None,
            end=None
        )

        assert isinstance(returns.index, PeriodIndex)
        assert returns.index.dtype == PeriodDtype(freq=offsets.BusinessDay())

    def given_any_ticker_symbol_when_reading_stock_data_should_return_only_returns_column(self):
        returns = StockReader(exchange_reader=None) \
            .stock_data(
            stock=Stock(ticker_symbol='EUNL.DE', currency=Currency.USD),
            start=None,
            end=None
        )

        assert returns.columns == ['Returns']

    def given_any_ticker_symbol_when_reading_stock_data_should_convert_adjusted_close_to_returns_as_relative_percentage(self, mocker: MockerFixture):
        def given():
            dates = ['2020-04-01', '2020-04-02', '2020-04-03']
            returns = (3.0, 1.5, 2.0)
            data = DataFrame(
                data={'Adj Close': returns},
                index=DatetimeIndex(data=dates, dtype='datetime64[ns]')
            )

            mock_reader = Mock()
            mock_reader.read.return_value = data

            mocker.patch(
                'f4ratk.stock.yahoo_reader',
                return_value=mock_reader
            )
        given()


        returns = StockReader(exchange_reader=None) \
            .stock_data(
            stock=Stock(ticker_symbol='EUNL.DE', currency=Currency.USD),
            start=None,
            end=None
        )

        assert returns['Returns']['2020-04-02'] == -50.0
        assert returns['Returns']['2020-04-03'] == approx(33.33333, rel=0.0001)

