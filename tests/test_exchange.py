from datetime import date

from pandas import PeriodIndex, PeriodDtype
from pandas.tseries import offsets

from f4ratk.exchange import ExchangeReader, Currency
from f4ratk.fama import Precision


def given_daily_precision_when_reading_data_should_return_daily_exchange_rate():
    result = ExchangeReader(precision=Precision.DAILY).exchange_data(
        currency=Currency.EUR,
        start=date(2020, 9, 16),
        end=date(2020, 9, 16)
    )

    assert isinstance(result.index, PeriodIndex)
    assert result.index.dtype == PeriodDtype(freq=offsets.BusinessDay())

    assert result['Exchange Rate']['2020-09-16'] == 1.1835


def given_monthly_precision_when_reading_data_should_return_monthly_exchange_rate():
    result = ExchangeReader(precision=Precision.MONTHLY).exchange_data(
        currency=Currency.EUR,
        start=date(2020, 9, 1),
        end=date(2020, 9, 30)
    )

    assert isinstance(result.index, PeriodIndex)
    assert result.index.dtype == PeriodDtype(freq=offsets.MonthEnd())

    assert len(result) == 1
    assert result['Exchange Rate']['2020-09'] == 1.1785


def should_rename_exchange_rate_column_to_generic_name():
    result = ExchangeReader(precision=Precision.DAILY).exchange_data(
        currency=Currency.EUR,
        start=date(2020, 9, 16),
        end=date(2020, 9, 16)
    )

    assert result.columns == ['Exchange Rate']
