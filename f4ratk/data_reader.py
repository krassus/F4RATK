from datetime import date, datetime, timedelta
from pathlib import Path

import requests_cache
from pandas import DataFrame, read_csv
from pandas_datareader.famafrench import FamaFrenchReader
from pandas_datareader.fred import FredReader
from pandas_datareader.yahoo.daily import YahooDailyReader


def _cached_session() -> requests_cache.CachedSession:
    cache_duration = timedelta(days=14)

    # noinspection PyTypeChecker
    session = requests_cache.CachedSession(
        cache_name='cache',
        backend='sqlite',
        expire_after=cache_duration
    )
    session.cache.remove_old_entries(datetime.utcnow() - cache_duration)

    return session


_session = _cached_session()


def fama_french_reader(returns_data: str) -> FamaFrenchReader:
    return FamaFrenchReader(symbols=returns_data, session=_session, start='1970')


def yahoo_reader(ticker_symbol: str, start: date, end: date) -> YahooDailyReader:
    return YahooDailyReader(symbols=ticker_symbol, start=start, end=end, session=_session)


def fred_reader(exchange_symbol: str, start: date, end: date) -> FredReader:
    return FredReader(symbols=exchange_symbol, start=start, end=end, session=_session)


class CsvFileReader:
    _HEADER = ('Dates', 'Returns')

    def __init__(self, path: Path):
        self._path = path
        self.validate_csv()

    def read(self) -> DataFrame:
        data = read_csv(
            self._path,
            index_col=0,
            names=self._HEADER,
            header=None,
            skiprows=1,  # no headers
        )

        return data

    def validate_csv(self):
        with open(self._path, 'r') as csv:
            missing = []
            headers = csv.readline().strip()

            for header in self._HEADER:
                if header not in headers:
                    missing.append(header)

            err = ', '.join(missing)

            if err:
                raise ValueError('CSV misses header(s) ' + err)


def csv_reader(path: Path) -> CsvFileReader:
    return CsvFileReader(path=path)
