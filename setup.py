from setuptools import setup

setup(
    name='F4RATK',
    version='2020.11',
    packages=['f4ratk'],
    url='https://codeberg.org/toroettg/4FRATK',
    license='MIT',
    author='Tobias Röttger',
    author_email='dev@roettger-it.de',
    description='A Fama/French Finance Factor Regression Analysis Toolkit.',
    entry_points={
        'console_scripts': [
            'f4ratk = f4ratk.f4ratk:main'
        ]
    },
    setup_requires=['pytest-runner'],
    tests_require=['pytest', 'pytest-cov'],
)
