import logging as log
from datetime import date
from enum import Enum, unique

from pandas import DataFrame

from f4ratk.data_reader import fred_reader
from f4ratk.fama import Precision
from f4ratk.shared import first_date, last_date


@unique
class Currency(Enum):
    EUR = 'Euro'
    USD = 'US Dollar'


class ExchangeReader:
    EXCHANGE_RATE_COLUMN = 'Exchange Rate'

    def __init__(self, precision: Precision):
        self._precision = precision

    def exchange_data(self, currency: Currency, start: date = None,
                      end: date = None) -> DataFrame:
        symbol = self._symbol(currency)

        data: DataFrame = fred_reader(exchange_symbol=symbol, start=start, end=end).read()

        data = data.rename(columns={symbol: self.EXCHANGE_RATE_COLUMN})

        data = self._convert_index_to_periods(data)

        data = data.dropna()

        log.info(f"Exchange data range : {first_date(data)} - {last_date(data)}")

        return data

    def _symbol(self, currency: Currency):
        if currency == Currency.EUR:
            if self._precision == Precision.DAILY:
                return 'DEXUSEU'
            elif self._precision == Precision.MONTHLY:
                return 'EXUSEU'

        raise NotImplemented

    def _convert_index_to_periods(self, data):
        frequency = 'B' if self._precision == Precision.DAILY else 'M'
        return data.to_period(freq=frequency)