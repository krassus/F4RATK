import argparse
from enum import Enum
from pathlib import Path

from f4ratk.file_reader import ValueFormat
from f4ratk.exchange import Currency
from f4ratk.fama import Region


class Input(Enum):
    FILE = 1
    TICKER = 2


def parseArguments() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description='A Fama/French Finance Factor Regression Analysis Toolkit',
        usage="""./main.py -f $FILE.csv -t $TICKER -c $CURRENCY -r $REGION"""
    )
    parser.add_argument(
        '-f',
        nargs='?',
        type=Path,
        dest='input',
        help='A csv file for analysis',
    )
    parser.add_argument(
        '-v',
        nargs='?',
        type=str,
        dest='value',
        help='Value format for CSV file',
        default=ValueFormat.PRICE,
    )
    parser.add_argument(
        '-t',
        nargs='?',
        type=str,
        dest='ticker',
        help='Ticker for the CSV file',
    )
    parser.add_argument(
        '-c',
        nargs='?',
        type=str,
        dest='currency',
        help='Currency for the Ticker',
    )
    parser.add_argument(
        '-r',
        nargs='?',
        type=str,
        dest='region',
        help='Region for the Ticker',
    )

    args = parser.parse_args()
    currency = validateCurrency(args)
    region = validateRegion(args)
    value_format = validateValueFormat(args)
    dataInput = validateDataInput(args)
    inputType = dataInput[0]
    data = dataInput[1]

    return argparse.Namespace(
        currency=currency,
        region=region,
        valueFormat=value_format,
        inputType=inputType,
        data=data,
    )


def validateValueFormat(args: argparse.Namespace) -> ValueFormat:
    if not args.value:
        raise ValueError('Value format (price or returns)')
    if args.value.lower() == 'price':
        value = ValueFormat.PRICE
    elif args.value.lower() == 'returns':
        value = ValueFormat.RETURNS
    else:
        raise ValueError('Only price and returns value formats are allowed')

    return value


def validateCurrency(args: argparse.Namespace) -> Currency:
    if not args.currency:
        raise ValueError('Provide currency (USD or EUR)')
    if args.currency.lower() == 'usd':
        currency = Currency.USD
    elif args.currency.lower() == 'eur':
        currency = Currency.EUR
    else:
        raise ValueError('Only USD and EUR currencies are allowed')

    return currency


def validateRegion(args: argparse.Namespace) -> Region:
    if not args.region:
        raise ValueError('Provide region (US or EU)')
    elif args.region.lower() == 'us':
        region = Region.US
    elif args.region.lower() == 'eu':
        region = Region.EU
    else:
        raise ValueError('Only US and EU regions are allowed')

    return region


def validateDataInput(args: argparse.Namespace) -> (str, str):
    if (not args.input) and (not args.ticker):
        raise ValueError('Either a file or a ticker, not neither')
    elif args.input and args.ticker:
        raise ValueError('Either from file or from ticker, not both')

    if args.input:
        dataInput = (Input.FILE, args.input)
    elif args.ticker:
        dataInput = (Input.TICKER, args.ticker.upper())
    else:
        raise ValueError('Something went very wrong')

    return dataInput
