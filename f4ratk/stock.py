import logging as log
from datetime import date

from pandas import DataFrame, merge, bdate_range

from f4ratk.data_reader import yahoo_reader
from f4ratk.exchange import Currency, ExchangeReader
from f4ratk.shared import last_date, first_date


class Stock:
    def __init__(self, ticker_symbol: str, currency: Currency = Currency.USD):
        self.ticker_symbol = ticker_symbol
        self.currency = currency


class StockReader:
    ADJUSTED_CLOSE_COLUMN = 'Adj Close'

    def __init__(self, exchange_reader: ExchangeReader):
        self._exchange_reader = exchange_reader

    def stock_data(self, stock: Stock, start: date = None, end: date = None) -> DataFrame:
        data = self._read_stock_data(stock, start, end)

        if stock.currency is not Currency.USD:
            data = self._convert_currency(data, stock.currency, start, end)

        data['Returns'] = self._calculate_returns_as_relative_percentage(data)

        log.debug(f"Stock data of symbol '{stock.ticker_symbol}' starts at: \n%s", data.head())

        self._stock_data_quality(data)

        return data[['Returns']].dropna()

    def _stock_data_quality(self, data: DataFrame) -> None:
        percent_filled: float = len(data) / len(bdate_range(start=first_date(data), end=last_date(data)))

        if percent_filled < 0.95:
            log.warning(f"Stock data contains only {percent_filled:.2%} of days")
        else:
            log.info(f"Stock data contains {percent_filled:.2%} of days")

    def _read_stock_data(self, stock: Stock, start: date, end: date) -> DataFrame:
        data: DataFrame = yahoo_reader(stock.ticker_symbol, start, end).read()
        return data.to_period(freq='B')

    def _convert_currency(self, data: DataFrame, currency: Currency, start: date, end: date) -> DataFrame:
        exchange_data = self._exchange_reader.exchange_data(currency, start, end)

        data = merge(
            data,
            exchange_data,
            left_index=True,
            right_index=True
        )

        source_currency_close_column = f"{self.ADJUSTED_CLOSE_COLUMN} ({currency.name})"

        data = data.rename(columns={self.ADJUSTED_CLOSE_COLUMN: source_currency_close_column})

        data[self.ADJUSTED_CLOSE_COLUMN] = data[source_currency_close_column] * \
                                           data[ExchangeReader.EXCHANGE_RATE_COLUMN]

        return data

    def _calculate_returns_as_relative_percentage(self, data: DataFrame) -> DataFrame:
        return data[[self.ADJUSTED_CLOSE_COLUMN]].pct_change() * 100
