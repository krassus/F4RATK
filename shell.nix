{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    # linting
    pkgs.python38Packages.flake8

    # debuggin
    pkgs.python38Packages.ipdb

    # testing
    pkgs.python38Packages.pytest
    pkgs.python38Packages.pytest-mock
    pkgs.python38Packages.pytestcov

    # dev
    pkgs.python38Packages.pandas
    pkgs.python38Packages.pandas-datareader
    pkgs.python38Packages.requests-cache
    pkgs.python38Packages.statsmodels
  ];
}
