from datetime import date

from pandas import DataFrame


def last_date(df: DataFrame) -> date:
    return df.tail(n=1).index.to_timestamp()[0].date()


def first_date(df: DataFrame) -> date:
    return df.head(n=1).index.to_timestamp()[0].date()
