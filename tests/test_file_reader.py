from unittest.mock import Mock

from pandas import DataFrame, DatetimeIndex, PeriodIndex, PeriodDtype
from pandas._libs.tslibs.period import Period
from pandas.tseries import offsets
from pytest import approx

from f4ratk.data_reader import CsvFileReader
from f4ratk.exchange import Currency
from f4ratk.fama import Precision
from f4ratk.file_reader import FileReader, ValueFormat


class TestFileReader:
    def given_a_descending_date_index_when_reading_data_should_sort_result_ascending(self):
        def given():
            dates = ['2020-12-31', '2020-11-01', '2020-10-15']
            returns = (3.00, 2.00, 1.00)
            data = DataFrame(
                data={'Returns': returns},
                index=DatetimeIndex(data=dates, dtype='datetime64[ns]')
            )

            return Mock(spec_set=CsvFileReader, read=Mock(return_value=data))

        mock_reader = given()

        result = FileReader(
            csv_reader=mock_reader,
            exchange_reader=None,
            currency=Currency.USD,
            value_format=ValueFormat.PRICE
        ).read()

        assert len(result) == 2
        assert result.head(n=1).index[0] == Period('2020-11-01', 'B')
        assert result.tail(n=1).index[0] == Period('2020-12-31', 'B')

    def given_a_daily_price_data_csv_file_when_reading_data_should_parse_first_column_to_daily_period_index(self):
        def given():
            dates = ['2020-12-31', '2020-11-01', '2020-10-15']
            returns = (3.00, 2.00, 1.00)
            data = DataFrame(
                data={'Returns': returns},
                index=DatetimeIndex(data=dates, dtype='datetime64[ns]')
            )

            return Mock(spec_set=CsvFileReader, read=Mock(return_value=data))

        mock_reader = given()

        result = FileReader(
            csv_reader=mock_reader,
            exchange_reader=None,
            currency=Currency.USD,
            value_format=ValueFormat.PRICE
        ).read()

        assert isinstance(result.index, PeriodIndex)
        assert result.index.dtype == PeriodDtype(freq=offsets.BusinessDay())
        assert result.columns == ['Returns']

    def given_a_daily_price_data_csv_file_when_reading_data_should_parse_second_column_to_relative_percentage_returns(
            self):
        def given():
            dates = ['2020-04-01', '2020-04-02', '2020-04-03']
            returns = (3.00, 2.00, 4.00)
            data = DataFrame(
                data={'Returns': returns},
                index=DatetimeIndex(data=dates, dtype='datetime64[ns]')
            )

            return Mock(spec_set=CsvFileReader, read=Mock(return_value=data))

        mock_reader = given()

        result = FileReader(
            csv_reader=mock_reader,
            exchange_reader=None,
            currency=Currency.USD,
            value_format=ValueFormat.PRICE
        ).read()

        assert result['Returns']['2020-04-02'] == approx(-33.33333, rel=0.0001)
        assert result['Returns']['2020-04-03'] == 100.0

    def given_a_monthly_price_data_csv_file_when_reading_data_should_parse_first_column_to_monthly_period_index(self):
        def given():
            dates = ['2020-04', '2020-05', '2020-06']
            returns = (3.00, 2.00, 4.00)
            data = DataFrame(
                data={'Returns': returns},
                index=DatetimeIndex(data=dates, dtype='datetime64[ns]')
            )

            return Mock(spec_set=CsvFileReader, read=Mock(return_value=data))

        mock_reader = given()

        result = FileReader(
            csv_reader=mock_reader,
            exchange_reader=None,
            currency=Currency.USD,
            value_format=ValueFormat.PRICE
        ).read(precision=Precision.MONTHLY)

        assert result['Returns']['2020-05'] == approx(-33.33333, rel=0.0001)
        assert result['Returns']['2020-06'] == 100.0