#!/usr/bin/env python3

import logging as log
from datetime import date
from pathlib import Path

from f4ratk.analyze import Analyzer
from f4ratk.data_reader import CsvFileReader
from f4ratk.exchange import Currency, ExchangeReader
from f4ratk.fama import FamaReader, Precision, Region
from f4ratk.file_reader import FileReader, ValueFormat
from f4ratk.stock import Stock, StockReader
from f4ratk.cli import parseArguments, Input

log.basicConfig(level=log.DEBUG)

log.getLogger("urllib3").setLevel(log.INFO)


class Config:
    def __init__(self, stock: Stock, region: Region):
        self.stock = stock
        self.region = region


class FileConfig:
    def __init__(
        self,
        path: Path,
        region: Region,
        currency: Currency = Currency.USD,
        value_format: ValueFormat = ValueFormat.PRICE
    ):
        self.path = path
        self.region = region
        self.currency = currency
        self.value_format = value_format


def analyzeStock(config: Config, start: str = None, end: str = None,
                 precision=Precision.DAILY) -> None:
    fama_reader = FamaReader()
    stock_reader = StockReader(ExchangeReader(precision))
    analyzer = Analyzer()

    analyzer.analyze(
        stock_reader.stock_data(config.stock, start, end),
        fama_reader.fama_data(region=config.region, precision=precision)
    )


def analyzeFile(config: FileConfig, start: str = None, end: str = None,
                precision=Precision.DAILY) -> None:
    fama_reader = FamaReader()
    file_reader = FileReader(
        csv_reader=CsvFileReader(path=config.path),
        exchange_reader=ExchangeReader(precision),
        currency=config.currency,
        value_format=config.value_format
    )

    analyzer = Analyzer()

    analyzer.analyze(
        file_reader.read(start, end, precision=precision),
        fama_reader.fama_data(region=config.region, precision=precision)
    )


def main():
    args = parseArguments()

    if args.inputType == Input.FILE:
        config = FileConfig(
            path=args.data,
            region=Region.US,
            currency=Currency.USD,
            value_format=ValueFormat.RETURNS
        )
        analyzeFile(config=config)

    elif args.inputType == Input.TICKER:
        config = Config(Stock(args.data, args.currency), region=args.region)
        analyzeStock(config=config, start=None, end=date.today())

    else:
        raise ValueError('Invalid input type')

    # Row format in ISO 8601 with optional day for monthly input and
    # price in US notation with arbitrary-precision decimal number value:
    # 'YYYY-MM[-DD],####.####', e.g.,
    #
    # '2020-09-11,183.53'
    # '2020-09,-63.53'
    # file_config_prices = FileConfig(
    #     path=Path('./input.csv'),
    #     region=Region.EU,
    #     currency=Currency.EUR
    # )
    #
    # zprv = Config(Stock('ZPRV.DE', Currency.USD), region=Region.US)
    # zprx = Config(Stock('ZPRX.F', Currency.EUR), region=Region.EU)
    # vmom = Config(Stock('VMOM.DE'), region=Region.DEVELOPED)
    # is3q = Config(Stock('IS3Q.DE'), region=Region.DEVELOPED)
    # is3r = Config(Stock('IS3R.DE'), region=Region.DEVELOPED)

    # analyzeStock(config=zprv, start=None, end="2020-11-22")

    # file_config = FileConfig(
    #     path=Path('./input.csv'),
    #     region=Region.EU,
    #     currency=Currency.EUR
    # )

    # analyzeFile(config=file_config)


if __name__ == '__main__':
    main()
